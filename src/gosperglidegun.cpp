#include "gosperglidergun.hpp"
#include <iostream>

  Gosper::Gosper(){

  Camp();
  setFigure("Gosper glider gun");
  setCycles(101);

  setGrade_camp(10,10,'O');
  setGrade_camp(11,10,'O');
  setGrade_camp(10,11,'O');
  setGrade_camp(11,11,'O');
  setGrade_camp(10,20,'O');
  setGrade_camp(11,20,'O');
  setGrade_camp(12,20,'O');
  setGrade_camp(13,21,'O');
  setGrade_camp(14,22,'O');
  setGrade_camp(14,23,'O');
  setGrade_camp(13,25,'O');
  setGrade_camp(12,26,'O');
  setGrade_camp(11,27,'O');
  setGrade_camp(10,26,'O');
  setGrade_camp(9,25,'O');
  setGrade_camp(8,23,'O');
  setGrade_camp(8,22,'O');
  setGrade_camp(9,21,'O');
  setGrade_camp(11,24,'O');
  setGrade_camp(11,26,'O');
  setGrade_camp(8,30,'O');
  setGrade_camp(9,30,'O');
  setGrade_camp(10,30,'O');
  setGrade_camp(10,31,'O');
  setGrade_camp(9,31,'O');
  setGrade_camp(8,31,'O');
  setGrade_camp(7,32,'O');
  setGrade_camp(7,34,'O');
  setGrade_camp(6,34,'O');
  setGrade_camp(11,34,'O');
  setGrade_camp(12,34,'O');
  setGrade_camp(11,32,'O');
  setGrade_camp(8,44,'O');
  setGrade_camp(9,44,'O');
  setGrade_camp(8,45,'O');
  setGrade_camp(9,45,'O');
}
