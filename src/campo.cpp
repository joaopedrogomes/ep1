#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "campo.hpp"
#include <unistd.h>
#define endl '\n';

// --- MÉTODO CONSTRUTOR ---

Camp::Camp(){

	// --- PREENCHIMENTO DA MATRIZ COM ESPAÇOS EM BRANCO ---

	for(int horizontal=0; horizontal<30; horizontal++){
		  for(int vertical=0; vertical<60; vertical++){
			     array_camp[horizontal][vertical] = '+';
      }
	}

}

Camp::~Camp(){} // MÉTODO DESTRUTOR

// --- Tipo de figura a ser mostrada na tela ---

string Camp::getFigure(){
  return figure;
}

void Camp::setFigure(string figure){
  this->figure = figure;
}

// --- Numero de iterações ---

int Camp::getCycles(){
  return cycles;
}
void Camp::setCycles(int cycles){
  this->cycles = cycles;
}

/// --- A função abaixo mostra de acordo com a figura escolida pelo usuario

char Camp::getGrade_camp(int horizontal, int vertical){
	return array_camp[horizontal][vertical];
}

// --- setando as dimensões da grade com um char para identificar as vidas 'O' ---

void Camp::setGrade_camp(int horizontal, int vertical, char Life){
	 array_camp[horizontal][vertical] = Life;

}

// --- Quantidade de celulas vivas para se aplicar as regras ---

int Camp::getCount_Neighbors(int horizontal, int vertical){
	return neighbors[horizontal][vertical];
}

/// --- Contando a quantidade de vizihos para devida aplicação das regras ---

void Camp::setCount_Neighbors(){

			int count=0;

	     for(int horizontal=0;horizontal<30;horizontal++){

	       for(int vertical=0;vertical<60;vertical++){

					 if(getGrade_camp(horizontal+1, vertical) == 'O'){
	           count++;
	         }

	         if(getGrade_camp(horizontal-1, vertical-1) == 'O'){
	           count++;
	         }

	         if(getGrade_camp(horizontal+1, vertical-1) == 'O'){
	           count++;
	         }
					 if(getGrade_camp(horizontal, vertical-1) == 'O'){
	           count++;
	         }
	         if(getGrade_camp(horizontal-1, vertical) == 'O'){
	           count++;
	         }

	         if(getGrade_camp(horizontal-1, vertical+1) == 'O'){
	           count++;
	         }
	         if(getGrade_camp(horizontal, vertical+1) == 'O'){
	           count++;
	         }
	         if(getGrade_camp(horizontal+1, vertical+1) == 'O'){
	           count++;
	         }

  			 neighbors[horizontal][vertical] = count;
				 count = 0;
		}
	}
}

/// --- Aplicação das regras ---

void Camp::Rules(){

	setCount_Neighbors();

    for(int horizontal=0;horizontal<30;horizontal++){
      for(int vertical=0;vertical<60;vertical++){

				/// --- Qualquer célula morta com exatamente três vizinhos vivos se torna uma célula viva. ---

				if(getGrade_camp(horizontal,vertical) == '+' && getCount_Neighbors(horizontal,vertical) == 3){
						setGrade_camp(horizontal,vertical,'O');
					}

				/// --- Qualquer célula viva com dois ou três vizinhos vivos continua no mesmo estado para a próxima geração. ---

				else if(getCount_Neighbors(horizontal,vertical)=='O' && getCount_Neighbors(horizontal,vertical) ==3){
								setGrade_camp(horizontal,vertical,'O');
					}

				/// --- Qualquer célula viva com menos de dois vizinhos vivos morre de solidão. ---

				else if(getGrade_camp(horizontal,vertical) == 'O'){
          	if(getCount_Neighbors(horizontal,vertical) < 2){
              	setGrade_camp(horizontal,vertical,'+');
            }
				/// --- Qualquer célula viva com mais de três vizinhos vivos morre de superpopulação. ---

          else if(getCount_Neighbors(horizontal,vertical) > 3){
              setGrade_camp(horizontal,vertical,'+');
            }
          }

      }
    }

	}

// --- Printando o campo do jogo ---

void Camp::printCamp(){


	   for(int cycles=0;cycles<101;cycles++){
			 
			 cout << string(50, '\n');

			for(int horizontal=0;horizontal<30;horizontal++){
        cout << "+";

			  for(int vertical=0;vertical<60;vertical++){
        cout << getGrade_camp(horizontal,vertical);
        }
        cout << endl;
      }

			cout << "GERAÇÕES:" << cycles << " !" << endl;
			// --- Aplica as regras ao campo ----
		  Rules();

			// --- Dar a impressão de animação ao jogo: biblioteca: unistd.h ---

			usleep(200000);
		}


}
