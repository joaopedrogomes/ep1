#include<bits/stdc++.h>
#include "campo.hpp"
#include "block.hpp"
#include "blinker.hpp"
#include "glider.hpp"
#include "gosperglidergun.hpp"
#define endl '\n'


using namespace std;

int main(){


        Camp array;
        Block block;
        Glider glider;
        Blinker blinker;
        Gosper gosperglidergun;

    // --- MENU INICIAL ---
    cout << endl;
    cout << "************************************************************************************************************" << endl;
    cout << "Welcome to the game of life!" << endl;
    cout << endl << endl;
    cout << "Regras: " << endl << endl;
    cout << "1. Qualquer célula viva com menos de dois vizinhos vivos morre de solidão." << endl << endl;
    cout << "2. Qualquer célula viva com mais de três vizinhos vivos morre de superpopulação." << endl << endl;
    cout << "3. Qualquer célula morta com exatamente três vizinhos vivos se torna uma célula viva." << endl << endl;
    cout << "4. Qualquer célula viva com dois ou três vizinhos vivos continua no mesmo estado para a próxima geração." << endl << endl;

    cout << endl << endl;

    cout << "Escolha a visualização na tela: " << endl << endl;
    cout << "1) GOSPER GLIDER GUN (GERADOR INFINITO DE GLIDERS)" << endl;
    cout << "2) BLOCK (FIGURA ESTÁTICA)" << endl;
    cout << "3) BLINKER(OSCILADOR)" << endl;
    cout << "4) GLIDER (NAVE ESPACIAL)" << endl;
    cout << "5) NENHUM (SAIR DO PROGRAMA) " << endl;

    int option;
  	cin >> option;

    while(option !=1 && option != 2 && option !=3 && option!=4 && option!=5){
      if((option >= 'a' && option <='z') || (option >= 'A' && option <='Z')){
        cout << "DIGITE UM NUMERO" << endl;
      }
      cout << "Opção inválida" << endl << "Digite um valor entre 1 e 5" << endl;
      cin >> option;
      cout << endl;
    }

    if(option == 1){
      array = gosperglidergun;
      array.printCamp();
    }

    if(option == 2){

      array = block;
      cout << "figura: " << array.getFigure() << endl << "Gerações: " << array.getCycles() << endl;
      array.printCamp();

    }

    if(option == 3){

      array = blinker;
      cout << "figura: " << array.getFigure() << endl << "Gerações: " << array.getCycles() << endl;
      array.printCamp();
    }

    if(option == 4){

      array = glider;

      cout << "figura: " <<array.getFigure() << endl << "Gerações: " <<array.getCycles() << endl;
      array.printCamp();

    }

    if(option == 5){
      return 0;
    }
  return 0;
}
