#ifndef CAMPO_HPP
#define CAMPO_HPP
#include <bits/stdc++.h>

using namespace std;

// --- CRIAÇÃO DA CLASSE CAMPO. ---

class Camp{

private:

     // --- Atributos do objeto campo. ---
    int cycles;
    string figure;
    char array_camp[30][60];
    int neighbors[30][60];

     // --- Métodos ---

   public:

      Camp();  //MÉTODO CONSTRUTOR(mesmo nome do objeto)
     ~Camp();

      // --- Métodos acessores ---

     int getCycles();
     void setCycles(int cycles);

     string getFigure();
     void setFigure(string figure);

     char getGrade_camp(int horizontal, int vertical);
     void setGrade_camp(int horizontal, int vertical, char Life);

     // --- Outros métodos ---

     void Rules(); // aplicação das regras
     void printCamp();


     int getCount_Neighbors(int horizontal, int vertical);
     void setCount_Neighbors();
};

#endif
