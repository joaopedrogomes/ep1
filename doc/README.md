# Nome: João Pedro Gomes
# Matricula: 140145842
# Matéria: Orientação a objetos

1. Abrir o terminal e executar o makefile
2. Escolher uma figura inicial para ser visualizada na tela
3. As figuras possuem gerações fixas de 100 ciclos.
4. O campo do jogo possui dimensões fixas para simulação padrão
5. As regras que foram utilizadas se encontram na página principal do programa.